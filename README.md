## Minimal nullnet broadcast example

Example in which some nodes turn off their radio before a broadcast transmission is complete as seen for nodes 2 and 3 in this image:

![png](nullnet_tsch_4nodes.png)

This behaviour occurs using a native installation as well as using the docker image with a number of nodes of at least 4. This behaviour occurs using sky nodes but not using cooja nodes.

**Makefile changes towards nullnet example in contiki-ng:**

```c
MAKE_MAC ?= MAKE_MAC_TSCH
```
instead of 

```c
MAKE_MAC ?= MAKE_MAC_CSMA
```
